var Szama = Szama || {}
Szama = (function () {

    var self = this;

    self.createOfficePolygon = function (map) {
        return L.polygon([
            [51.11677, 16.99658],
            [51.11659, 16.99680],
            [51.11656, 16.99673],
            [51.11652, 16.99678],
            [51.11643, 16.99659],
            [51.11681, 16.99611],
            [51.1169, 16.9963],
            [51.11674, 16.9965]
        ], {
            fillOpacity: 0.9
        }).addTo(map)
    }

    self.buildFoodPoints = function (map) {
        $.getJSON('data/food-points.json')
            .then(function (data) {              
                for (var i in data) {
                    var foodPoint = data[i]
                    addFoodPointToMap(map, foodPoint)
                }
            })
    }

    var addFoodPointToMap = function (map, point) {
        var marker = L.marker([point.x, point.y]).addTo(map)
        marker.bindPopup('<p><b>' + point.title + '</b></p><p>' + point.description + '</p>')
    }

    return {
        start: function (id) {
            var map = L.map(id).setView([51.1152, 16.9947], 16)
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map)
            var polygon = self.createOfficePolygon(map)

            self.buildFoodPoints(map)
        }
    }
}())